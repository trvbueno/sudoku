#ifndef ENTREES_H
#define ENTREES_H

#include <stdio.h>
#include <stdlib.h>

#include <affichage.h>
#include <gestion.h>

Mode_Execution lireModeExecution();
T_sudoku lireGrilleConsole();
T_sudoku lireGrilleFichier(const char *chemin);
void ecrireGrilleFichier(const char *chemin, T_sudoku sdk);
void attendreExecution();

#endif /* ENTREES_H */

