#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <stdio.h>

#include <structures.h>
#include <utilitaires.h>

void afficherGrille(T_sudoku sdk);
void afficherCandidats(T_case cell);

#endif /* AFFICHAGE_H */
