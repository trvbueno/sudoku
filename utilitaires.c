#include <utilitaires.h>

/*
 * Les fonctions suivantes sont suffisament explicites pour se passer de documentation.
 */
int premiereLigneRegion(int reg)
{
    return (3*(reg/3));
}

int premiereColonneRegion(int reg)
{
    return (3*(reg%3));
}

int colonneCase(int cas)
{
    return (cas % 9);
}

int ligneCase(int cas)
{
    return (cas / 9);
}

int regionCase(int cas)
{
    return ((3*(ligneCase(cas)/3)) + (colonneCase(cas)/3));
}

int numeroCase(int lig, int col)
{
    return ((lig*9) + col);
}

int testerAccessibiliteFichier(const char *chemin)
{
    FILE * fichier = fopen(chemin, "r");
    if (fichier == NULL) return 0;
        else { return 1; fclose(fichier); }
}

/*
 * Fonction : Recherche de l'indice d'une valeur dans un tableau.
 * Arguments : Un tableau, sa taille entière positive, une valeur entière.
 * Retourne : L'indice de la valeur présente dans le tableau, la taille du tableau sinon.
 */

/*
 * Spécification
 *
 * PE : N entier naturel, TAB[N] tableau d'entiers de taille N, M entier
 * rechercheIndice(TAB, N, M, n);
 * PS : si il existe au moins un i appartenant à [0,N] tq TAB[i] = M, alors n = i
 *      sinon n = N
 */
int rechercheIndice(int *tab, unsigned int n, int m)
{
    unsigned int i=0;
    int trouve=0;
    while(i < n)
    {
        if(tab[i] == m) trouve++;
        else i++;
    }
    return i;
}

/*
 * Fonction : Vérifie si un candidat est présent dans une case.
 * Arguments : Une case, un candidat entier.
 * Retourne : 1 si le candidat est présent, 0 sinon.
 */
int candidatPresent(T_case cell, int ctd)
{
    if(cell.n_candidats == rechercheValeur(cell.candidats, cell.n_candidats, ctd)) return 0;
        else return 1;
}

/*
 * Fonction : Extraie une ligne d'une grille de sudoku.
 * Arguments : Une grille de sudoku, un tableau où écrire les cases, un numéro de ligne entier.
 * Note : la fonction ne procède pas à des copies de cases, elle retourne des pointeurs vers celles-ci.
 */
void extraireLigne(T_sudoku *sdk, T_case **ligne, int lig)
{
    for(int i=0; i<9; i++)
        ligne[i] = &((*sdk).grille[numeroCase(lig,i)]);
}

/*
 * Fonction : Extraie une colonne d'une grille de sudoku.
 * Arguments : Une grille de sudoku, un tableau où écrire les cases, un numéro de colonne entier.
 * Note : la fonction ne procède pas à des copies de cases, elle retourne des pointeurs vers celles-ci.
 */
void extraireColonne(T_sudoku *sdk, T_case **colonne, int col)
{
    for(int i=0; i<9; i++)
        colonne[i] = &((*sdk).grille[numeroCase(i,col)]);
}

/*
 * Fonction : Extraie une région d'une grille de sudoku.
 * Arguments : Une grille de sudoku, un tableau où écrire les cases, un numéro de région entier.
 * Note : la fonction ne procède pas à des copies de cases, elle retourne des pointeurs vers celles-ci.
 */
void extraireRegion(T_sudoku *sdk, T_case **region, int reg)
{
    int k=0;
    for(int i=premiereLigneRegion(reg); i<premiereLigneRegion(reg)+3; i++)
        for(int j=premiereColonneRegion(reg); j<premiereColonneRegion(reg)+3; j++)
        {
            region[k] = &((*sdk).grille[numeroCase(i,j)]);
            k++;
        }
}
