#ifndef GESTION_H
#define GESTION_H

#include <structures.h>

void initialiserGrille(T_sudoku *sdk);
void viderCandidats(T_case *cell);
void initialiserCandidats(T_sudoku *sdk);
int rechercheValeur(int tab[], int taille, int val);
int supprimerCandidat(T_case *cell, int val);

#endif /* GESTION_H */
