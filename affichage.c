#include <affichage.h>

/*
 * Les fonctions suivantes sont suffisament explicites pour se passer de documentation.
 */

void afficherBordureHaut()
{
    printf("┏━━━┯━━━┯━━━┳━━━┯━━━┯━━━┳━━━┯━━━┯━━━┓\n");
}

void afficherBordureBas()
{
    printf("┗━━━┷━━━┷━━━┻━━━┷━━━┷━━━┻━━━┷━━━┷━━━┛\n");
}

void afficherBordureInterne()
{
    printf("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
}

void afficherBordureInterneGrasse()
{
    printf("┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫\n");
}

void afficherLigne(int lig, T_sudoku sdk)
{
    printf("┃");
    for(int col=0; col<9; col++)
    {
        int nb = sdk.grille[numeroCase(lig,col)].val;
        if(nb == 0) printf("   ");
            else printf(" %d ", nb);
        if(col != 8)
        {
            if((col+1) % 3 == 0) printf("┃");
            else printf("│");
        }
    }
    printf("┃\n");
}

void afficherGrille(T_sudoku sdk)
{
    afficherBordureHaut();
    for(int lig=0; lig<9; lig++)
    {
        afficherLigne(lig, sdk);
        if(lig != 8)
        {
            if((lig+1) % 3 == 0) afficherBordureInterneGrasse();
            else afficherBordureInterne();
        }
    }
    afficherBordureBas();
}

void afficherCandidats(T_case cell)
{
    for(int i=0; i<cell.n_candidats; i++)
    {
        printf("%d ", cell.candidats[i]);
    }
    printf("\n");
}
