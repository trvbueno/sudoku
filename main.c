#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <structures.h>
#include <gestion.h>
#include <affichage.h>
#include <entrees.h>

#include <regles/R1.h>
#include <regles/R2.h>
#include <regles/R3.h>
#include <regles/R4.h>

int main(int argc, char *argv[])
{
    T_sudoku sdk;
    initialiserGrille(&sdk);

    int sortie_fichier = 0;
    const char *chemin = NULL;

    /* Si un argument est passé en paramètre */
    if(argc == 2)
    {
        chemin = argv[1];

        /* Si le paramètre est un fichier accessible */
        if(testerAccessibiliteFichier(chemin))
        {
            sdk = lireGrilleFichier(chemin);
            sortie_fichier = 1;
        } else {
            printf("%s n'est pas un fichier accessible, impossible de lire une grille de sudoku depuis ce fichier.",chemin);
            return 1;
        }

    } else {
        sdk = lireGrilleConsole();
    }

    Mode_Execution mode = lireModeExecution();

    afficherGrille(sdk);
    initialiserCandidats(&sdk);

    int R1=1, R2=1, R3=1, R4=1;
    while(R1+R2+R3 != 0)
    {
        R1 = executer_R1(&sdk, mode);
        R2 = executer_R2(&sdk);
        R3 = executer_R3(&sdk, mode);
        R4 = executer_R4(&sdk);
        printf("%d candidat(s) retiré(s) de cases par R2 \n", R2);
        printf("%d case(s) révelées par R1 \n", R1);
        printf("%d case(s) révelées par R3 \n", R3);
        printf("%d candidat(s) retiré(s) de lignes/colonnes par R4 \n", R4);
        afficherGrille(sdk);
    }

    char *chemin_sortie = (char *)chemin;
    if(sortie_fichier) ecrireGrilleFichier(strcat(chemin_sortie,".solution"), sdk);

    return 0;
}
