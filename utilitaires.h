#ifndef UTILITAIRES_H
#define UTILITAIRES_H

#include <stdio.h>

#include <structures.h>
#include <gestion.h>

int premiereLigneRegion(int reg);
int premiereColonneRegion(int reg);
int colonneCase(int cas);
int ligneCase(int cas);
int regionCase(int cas);
int numeroCase(int lig, int col);
int testerAccessibiliteFichier(const char *chemin);
int rechercheIndice(int *tab, unsigned int taille, int n);
int candidatPresent(T_case cell, int ctd);
void extraireLigne(T_sudoku *sdk, T_case **ligne, int lig);
void extraireColonne(T_sudoku *sdk, T_case **colonne, int col);
void extraireRegion(T_sudoku *sdk, T_case **region, int reg);

#endif /* UTILITAIRES_H */
