#include <gestion.h>

/*
 * Fonction : Initialise une grille de sudoku à zéro ( valeurs et nombre de candidats ).
 * Arguments : Une grille de sudoku.
 */
void initialiserGrille(T_sudoku *sdk)
{
    for(int i=0; i<81; i++)
    {
        (*sdk).grille[i].val = 0;
        (*sdk).grille[i].n_candidats = 0;
    }
}

/*
 * Fonction : Supprime tous les candidats d'une case donnée.
 * Arguments : Une case.
 */
void viderCandidats(T_case *cell)
{
    (*cell).n_candidats = 0;
    for(int j=0; j<9; j++)
    {
        (*cell).candidats[j] = 0;
    }
}

/*
 * Fonction : Initialise les candidats des cases d'une grille de sudoku.
 * Arguments : Une grille de sudoku.
 */
void initialiserCandidats(T_sudoku *sdk)
{
    for(int i=0; i<81; i++)
    {
        if((*sdk).grille[i].val == 0)
        {
            (*sdk).grille[i].n_candidats = 9;
            for(int j=0; j<9; j++)
            {
                (*sdk).grille[i].candidats[j] = j+1;
            }
        } else {
            viderCandidats(&((*sdk).grille[i]));
        }
    }
}

/*
 * Fonction : Recherche de l'indice d'une valeur dans un tableau de taille donnée.
 * Arguments : Tableau dans lequel chercher, sa taille, et la valeur entière à chercher.
 * Retourne : La taille du tableau si la valeur cherchée n'est pas dans celui-ci.
 */
int rechercheValeur(int tab[], int taille, int val)
{
    for(int i=0; i<taille; i++)
    {
        if(tab[i] == val) return i;
    }
    return taille;
}

/*
 * Fonction : Suppression d'une valeur de la liste des candidats d'une case.
 * Arguments : Une case, une valeur entière.
 * Retourne : 0 si le candidat n'était pas présent dans la liste, 1 sinon.
 */
int supprimerCandidat(T_case *cell, int val)
{
    int indice = rechercheValeur((*cell).candidats, (*cell).n_candidats, val);

    if(indice != (*cell).n_candidats)
    {
        /* On décale d'une position vers la gauche les candidats suivants la valeur à supprimer */
        for(int i=indice; i<(*cell).n_candidats; i++)
            (*cell).candidats[i] = (*cell).candidats[i+1];

        (*cell).n_candidats -= 1;

        /* On remplace la fin du tableau par des 0, par soucis de lisibilité lors de l'inspection des variables */
        for(int i=(*cell).n_candidats; i<9; i++)
            (*cell).candidats[i] = 0;

        return 1;
    }
    return 0;
}

