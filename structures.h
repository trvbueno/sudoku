#ifndef STRUCTURES_H
#define STRUCTURES_H

typedef struct {
    int val;
    int n_candidats;
    int candidats[9];
} T_case;

typedef struct {
    T_case grille[81];
} T_sudoku;

typedef enum
{
    NORMAL, PAS_A_PAS
} Mode_Execution;

#endif /* STRUCTURES_H */
