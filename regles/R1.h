#ifndef R1_H
#define R1_H

#include <stdio.h>

#include <structures.h>
#include <gestion.h>
#include <entrees.h>
#include <affichage.h>

int executer_R1(T_sudoku *sdk, Mode_Execution mode);

#endif /* R1_H */
