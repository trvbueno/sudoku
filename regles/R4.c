#include <regles/R4.h>

/*
 * Fonction : Supprime des cases de la ligne/colonne ne faisant pas partie de la région donné, un candidat de la liste des candidats
 * Arguments : Un groupement ( ligne/colonne ), un numéro de ligne/colonne, la région à exclure, un candidat entier
 */
void supprimerCandidatsLigne(T_case **ligne, int lig, int regionExclue, int candidat)
{
    for(int i=0; i<9; i++)
    {
        if(regionCase(numeroCase(lig,i)) != regionExclue)
            supprimerCandidat(ligne[i],candidat);
    }
}

void supprimerCandidatsColonne(T_case **colonne, int col, int regionExclue, int candidat)
{
    for(int i=0; i<9; i++)
    {
        if(regionCase(numeroCase(i,col)) != regionExclue)
            supprimerCandidat(colonne[i],candidat);
    }
}

/*
 * Fonction : Si un candidat n'est présent que dans une ligne/colonne d'une région,
 *            on peut le supprimer de la liste des candidats des autres cases de cette ligne/colonne
 * Arguments : Une grille, une région
 * Retourne : Le nombre de fois où des candidats ont été retirés d'une ligne/colonne pour cette région.
 */
int traitement_R4(T_sudoku *sdk, int reg)
{
    T_case *region[9];  extraireRegion(sdk, region, reg);
    int premLig = premiereLigneRegion(reg);
    int premCol = premiereColonneRegion(reg);
    int cpt=0;

    /* Pour chaque ligne de la région */
    for(int i=premLig; i<premLig+3; i++)
    {
        T_case *ligne[9]; extraireLigne(sdk, ligne, i);
        for(int j=premCol; j<premCol+3; j++)
        {
            /* Pour chaque case de cette ligne dans la région */
            T_case *cell = &((*sdk).grille[numeroCase(i,j)]);

            /* Pour chaque candidat de la case */
            for(int c=0; c<(*cell).n_candidats; c++)
            {
                int cdt1=(*cell).candidats[c];
                int presence=0;

                /* On vérifie que ce candidat n'est présent dans aucune case des autres lignes de la région */
                for(int k=premLig; k<premLig+3; k++)
                {
                    if(k != i)
                    {
                        for(int l=premCol; l<premCol+3; l++)
                        {
                            //printf("R4 -> reg %d, case (%d,%d) n°%d, étude de (%d,%d) n°%d pour %d\n",reg,i,j,numeroCase(i,j),k,l,numeroCase(k,l),cdt1);
                            T_case *case_etudiee = &((*sdk).grille[numeroCase(k,l)]);

                            /* Pour chaque candidat de la case */
                            for(int d=0; d<(*case_etudiee).n_candidats; d++)
                                if((*case_etudiee).candidats[d] == cdt1) presence++;
                        }
                    }
                }

                if(presence==0)
                {
                    //printf("Region %d: on peut supprimer %d de la ligne %d\n",reg,cdt1,i);
                    supprimerCandidatsLigne(ligne,i,reg,cdt1);
                    cpt++;
                }
            }
        }
    }

    /* ###################################################################### */
    /* Le code qui suit est équivalent au code précédent, adapté aux colonnes */
    /* ###################################################################### */

    /* Pour chaque colonne de la région */
    for(int i=premCol; i<premCol+3; i++)
    {
        T_case *colonne[9]; extraireColonne(sdk, colonne, i);
        for(int j=premLig; j<premLig+3; j++)
        {
            /* Pour chaque case de cette ligne dans la région */
            T_case *cell = &((*sdk).grille[numeroCase(j,i)]);

            /* Pour chaque candidat de la case */
            for(int c=0; c<(*cell).n_candidats; c++)
            {
                int cdt1=(*cell).candidats[c];
                int presence=0;

                /* On vérifie que ce candidat n'est présent dans aucune case des autres colonnes de la région */
                for(int k=premCol; k<premCol+3; k++)
                {
                    if(k != i)
                    {
                        for(int l=premLig; l<premLig+3; l++)
                        {
                            T_case *case_etudiee = &((*sdk).grille[numeroCase(l,k)]);

                            /* Pour chaque candidat de la case */
                            for(int d=0; d<(*case_etudiee).n_candidats; d++)
                                if((*case_etudiee).candidats[d] == cdt1) presence++;
                        }
                    }
                }

                if(presence==0)
                {
                    //printf("Region %d: on peut supprimer %d de la colonne %d\n",reg,cdt1,i);
                    supprimerCandidatsColonne(colonne,i,reg,cdt1);
                    cpt++;
                }
            }
        }
    }

    return cpt;
}

/*
 * Fonction : Execute le traitement de la règle 4.
 * Arguments : Une grille de sudoku
 * Retourne : Le nombre de fois où des candidats ont été retirés d'une ligne/colonne
 */
int executer_R4(T_sudoku *sdk)
{
    int cpt = 0;

    /* Pour chaque région */
    for(int i=0; i<9; i++)
    {
        cpt += traitement_R4(sdk, i);
    }
    return cpt;
}
