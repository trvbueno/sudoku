#include <regles/R1.h>

/*
 * Fonction : Remplace toutes les valeurs des cases non remplies et qui n'ont qu'un seul candidat par ce candidat.
 * Arguments : Une case.
 * Retourne : 1 si la case a été remplacée, 0 sinon.
 */
int traitement_R1(T_case *cell)
{
    if((*cell).n_candidats == 1)
    {
        (*cell).val = (*cell).candidats[0];
        supprimerCandidat(cell, (*cell).candidats[0]);
        return 1;
    }
    return 0;
}

/*
 * Fonction : Execute le traitement de la règle 1.
 * Arguments : Une grille de sudoku, le mode d'exécution de la règle.
 * Retourne : Le nombre de cases remplies dans la grille lors de l'exécution.
 */
int executer_R1(T_sudoku *sdk, Mode_Execution mode)
{
    int cpt = 0;
    for(int i=0; i<81; i++)
    {        
        if(mode == NORMAL)
            cpt += traitement_R1(&((*sdk).grille[i]));

        if(mode == PAS_A_PAS)
        {
            if(traitement_R1(&((*sdk).grille[i])) == 1)
            {
                cpt++;
                afficherGrille(*sdk);
                printf("Règle 1 -> Case (%d;%d) modifiée\n", ligneCase(i), colonneCase(i));
                attendreExecution();
            }
        }
    }
    return cpt;
}
