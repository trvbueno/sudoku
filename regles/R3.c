#include <regles/R3.h>

/*
 * Fonction : Vérifie l'unicité d'un candidat d'une case pour un groupement de case donné.
 * Arguments : Une case, un groupement de cases, un candidat entier.
 * Retourne : 1 si le candidat est unique dans le groupement, 0 sinon.
 */
int verifierUniciteCandidat(T_case *caseEtudiee, T_case **groupementCases, int ctd)
{
    int occurences_candidats_communs = 0;
    for(int i=0; i<9; i++)
    {
        T_case *case_traitee = groupementCases[i];
        if(caseEtudiee != case_traitee)
            occurences_candidats_communs += candidatPresent(*case_traitee, ctd);
    }
    return (!occurences_candidats_communs);
}

/*
 * Fonction : Remplace une case par un de ses candidats, si celui-ci est unique dans l'un des groupements auxquels appartient celle-ci.
 * Arguments : Une grille, une case, le numéro de la case dans la grille.
 * Retourne : 1 si la case a été remplie, 0 sinon.
 */
int traitement_R3(T_sudoku *sdk, T_case *cell, int numCase)
{
    assert(numCase >= 0 && numCase <= 80);
    int col = colonneCase(numCase);
    int lig = ligneCase(numCase);
    int reg = regionCase(numCase);

    /* Parmis tous les candidats de la case courante à traiter */
    for(int c=0; c<(*cell).n_candidats; c++)
    {
        int candidat = (*cell).candidats[c];

        /* On extraie la ligne, colonne et région de cette case */
        T_case *ligne[9];   extraireLigne(sdk, ligne, lig);
        T_case *colonne[9]; extraireColonne(sdk, colonne, col);
        T_case *region[9];  extraireRegion(sdk, region, reg);

        /* On vérifie que le candidat considéré est unique pour chaque région de la case */
        int candidatUniqueGroupement = verifierUniciteCandidat(cell, ligne, candidat)   ||
                                       verifierUniciteCandidat(cell, colonne, candidat) ||
                                       verifierUniciteCandidat(cell, region, candidat);

        /* Et si le candidat est bien unique dans au moins une des régions ... */
        if(candidatUniqueGroupement)
        {
            /* ... la case prend sa valeur */
            (*cell).val = candidat;
            viderCandidats(cell);

            /* et il faut _impérativement_ exécuter la règle 2 après chaque modification de la grille,
               car la règle 3 est _basée_ sur la validité de la liste des candidats pour chaque case. */
            executer_R2(sdk);

            return 1;
        }

    }
    return 0;
}

/*
 * Fonction : Execute le traitement de la règle 3.
 * Arguments : Une grille de sudoku, le mode d'exécution de la règle.
 * Retourne : Le nombre de cases remplies dans la grille lors de l'exécution.
 */
int executer_R3(T_sudoku *sdk, Mode_Execution mode)
{
    int cpt = 0;
    for(int i=0; i<81; i++)
    {
        if(mode == NORMAL)
            cpt += traitement_R3(sdk, &((*sdk).grille[i]), i);

        if(mode == PAS_A_PAS)
        {
            int cnt = traitement_R3(sdk, &((*sdk).grille[i]), i);
            if(cnt)
            {
                cpt++;
                afficherGrille(*sdk);
                printf("Règle 3 -> Case (%d;%d) modifiée\n", ligneCase(i), colonneCase(i));
                attendreExecution();
            }
        }
    }
    return cpt;
}
