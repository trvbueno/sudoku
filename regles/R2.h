#ifndef R2_H
#define R2_H

#include <stdio.h>
#include <assert.h>

#include <structures.h>
#include <gestion.h>
#include <entrees.h>
#include <affichage.h>

int executer_R2(T_sudoku *sdk);

#endif /* R2_H */
