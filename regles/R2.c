#include <regles/R2.h>

/*
 * Fonction : Supprime de la liste des candidats d'une case les valeurs des cases remplies présentes dans le même groupement de cases.
 * Arguments : Une case, et son numéro de case entier.
 * Retourne : Le nombre de candidats supprimés.
 */
int traitement_R2(T_sudoku *sdk, T_case *cell, int numCase)
{
    assert(numCase >= 0 && numCase <= 80);

    int cpt = 0;
    int col = colonneCase(numCase);
    int lig = ligneCase(numCase);
    int reg = regionCase(numCase);

    /* On itère parmis toutes les cases de la ligne courante */
    for(int i=0; i<9; i++)
        if(supprimerCandidat(cell,(*sdk).grille[numeroCase(lig,i)].val)) cpt++;

    /* On itère parmis toutes les cases de la colonne courante */
    for(int i=0; i<9; i++)
        if(supprimerCandidat(cell,(*sdk).grille[numeroCase(i,col)].val)) cpt++;

    /* On itère parmis toutes les cases de la région courante */
    for(int i=premiereLigneRegion(reg); i<premiereLigneRegion(reg)+3; i++)
        for(int j=premiereColonneRegion(reg); j<premiereColonneRegion(reg)+3; j++)
            if(supprimerCandidat(cell,(*sdk).grille[numeroCase(i,j)].val)) cpt++;

    return cpt;
}

/*
 * Fonction : Execute le traitement de la règle 2.
 * Arguments : Une grille de sudoku
 * Retourne : Le nombre de candidats supprimés dans la grille lors de l'exécution.
 */
int executer_R2(T_sudoku *sdk)
{
    int cpt = 0;
    for(int i=0; i<81; i++)
    {
        cpt += traitement_R2(sdk, &((*sdk).grille[i]), i);
    }
    return cpt;
}
