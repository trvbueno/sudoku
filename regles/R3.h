#ifndef R3_H
#define R3_H

#include <stdio.h>
#include <assert.h>

#include <structures.h>
#include <gestion.h>
#include <entrees.h>
#include <affichage.h>
#include <utilitaires.h>
#include <regles/R2.h>

int executer_R3(T_sudoku *sdk, Mode_Execution mode);

#endif /* R3_H */
