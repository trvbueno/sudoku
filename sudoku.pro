TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -std=c99

SOURCES += main.c \
    affichage.c \
    entrees.c \
    gestion.c \
    utilitaires.c \
    regles/R1.c \
    regles/R2.c \
    regles/R3.c \
    regles/R4.c \
    regles/R5.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    structures.h \
    affichage.h \
    entrees.h \
    gestion.h \
    utilitaires.h \
    R1.h \
    regles/R1.h \
    regles/R2.h \
    regles/R3.h \
    regles/R4.h \
    regles/R5.h

