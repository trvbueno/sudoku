#include <entrees.h>

/*
 * Fonction : Lit un mode d'exécution depuis la console.
 * Retourne : Un mode d'exécution.
 */
Mode_Execution lireModeExecution()
{
    printf("Voulez vous activer le mode pas à pas (o/n) ? ");
    if(getchar() == 'o') return PAS_A_PAS;
        else return NORMAL;
}

/*
 * Fonction : Lit une grille de sudoku depuis l'entrée standard de la console. La grille doit être une suite de 81 chiffres.
 *            Si un des chiffres est invalide, il est remplacé par 0.
 * Retourne : La grille de sudoku.
 */
T_sudoku lireGrilleConsole()
{
    T_sudoku gr;
    initialiserGrille(&gr);

    printf("Grille : \n");
    for(int lig=0; lig<9; lig++)
        for(int col=0; col<9; col++)
        {
            int chiffre; scanf("%d",&chiffre);
            if(chiffre < 0 || chiffre > 9) chiffre = 0;
            gr.grille[numeroCase(lig,col)].val = chiffre;
        }
    return gr;

}

/*
 * Fonction : Lit une grille de sudoku depuis un fichier. La grille doit être une suite de 81 chiffres.
 *            Si un des chiffres est invalide, il est remplacé par 0. Si le fichier ne contient pas 81 chiffres, le reste de la grille vaut 0.
 * Arguments : Le chemin d'accès du fichier: une chaine de caractère constante
 * Retourne : La grille de sudoku.
 */
T_sudoku lireGrilleFichier(const char *chemin)
{
    FILE *fichier_sudoku = fopen(chemin, "r");
    T_sudoku gr;
    initialiserGrille(&gr);
    int lecture=1;

    for(int lig=0; lig<9 && lecture; lig++)
        for(int col=0; col<9 && lecture; col++)
        {
            int chiffre;
            lecture = fscanf(fichier_sudoku, "%d", &chiffre);
            if(chiffre < 0 || chiffre > 9) chiffre = 0;
            gr.grille[numeroCase(lig,col)].val = chiffre;
        }
    fclose(fichier_sudoku);
    return gr;
}

/*
 * Fonction : Ecrit une grille de sudoku dans un fichier, comme une suite de 81 chiffres.
 * Arguments : Le chemin d'accès du fichier: une chaine de caractère constante, une grille de sudoku
 */
void ecrireGrilleFichier(const char *chemin, T_sudoku sdk)
{
    FILE *fichier_sudoku = fopen(chemin, "w");
    for(int lig=0; lig<9; lig++)
    {
        for(int col=0; col<9; col++)
        {
            fprintf(fichier_sudoku, "%d ",sdk.grille[numeroCase(lig,col)].val);
        }
        fprintf(fichier_sudoku, "\n");
    }
    fclose(fichier_sudoku);
}

/*
 * Fonction : Met en pause l'exécution du programme, jusqu'à ce que la touche entrée soit pressée par l'utilisateur.
 */
void attendreExecution()
{
    printf("< Appuyer sur entrée pour continuer >\n");
    while (getchar() != '\n');
    getchar();
}
